import json
import os
import re
from dataclasses import dataclass, asdict
from pathlib import Path
from typing import Protocol, Union, Optional, Dict

from pointy.api import provide, inject, construct, Marker


@dataclass
class Todo:
    id: Optional[int]
    name: str
    completed: bool = False


class TodoDAO(Protocol):
    def save(self, todo: Todo) -> Todo:
        ...

    def find(self, todo_id: int) -> Todo:
        ...

    def update(self, todo: Todo) -> Todo:
        ...

    def delete(self, identifier: Union[int, Todo]):
        ...


@provide(TodoDAO)
class JSONTodoDAO(TodoDAO):
    _base_directory: Path

    def __init__(self, name: Optional[str] = "todo-data"):
        self._base_directory = Path(f"/tmp/{name}")
        self._base_directory.mkdir(parents=True, exist_ok=True)

    def _todo_path(self, todo_id: int, require_exists: bool = False):
        path = self._base_directory / f"todo-{todo_id}.json"
        if require_exists and not path.exists():
            raise FileNotFoundError(path)

        return path

    def _id_path_mapping(self) -> Dict[int, Path]:
        def id_from_path(p: Path):
            if match := re.search(r"todo-(\d+)\.json", p.name):
                return int(match.group(1))

            return -1

        return {id_from_path(x): x for x in self._base_directory.glob("*.json")}

    def _generate_id(self) -> int:
        indices = list(self._id_path_mapping().keys())
        if not indices:
            indices.append(-1)

        return max(indices) + 1

    def save(self, todo: Todo) -> Todo:
        todo.id = todo.id or self._generate_id()

        with self._todo_path(todo.id).open("w+", encoding="UTF-8") as fp:
            json.dump(asdict(todo), fp, indent=2)

        return todo

    def find(self, todo_id: int) -> Todo:
        path = self._todo_path(todo_id, require_exists=True)

        with path.open("r", encoding="UTF-8") as fp:
            return Todo(**json.load(fp))

    def update(self, todo: Todo) -> Todo:
        self._todo_path(todo.id, require_exists=True)

        todo = Todo(**{
            **asdict(self.find(todo.id)),
            **asdict(todo)
        })

        return self.save(todo)

    def delete(self, identifier: Union[int, Todo]):
        if isinstance(identifier, Todo):
            identifier = identifier.id

        os.remove(self._todo_path(identifier, require_exists=True))


class MockTodoDOA(TodoDAO):
    _storage = Dict[int, Todo]

    def __init__(self, *_args, **_kwargs):
        self._storage = {}

    def _generate_id(self) -> int:
        indices = list(self._storage.keys())
        if not indices:
            indices.append(-1)

        return max(indices) + 1

    def save(self, todo: Todo) -> Todo:
        todo.id = todo.id or self._generate_id()
        self._storage[todo.id] = todo

        return todo

    def find(self, todo_id: int) -> Todo:
        return self._storage[todo_id]

    def update(self, todo: Todo) -> Todo:
        todo = Todo(**{
            **asdict(self._storage[todo.id]),
            **asdict(todo)
        })

        return self.save(todo)

    def delete(self, identifier: Union[int, Todo]):
        self._storage.pop(identifier.id if isinstance(identifier, Todo) else identifier)

    @classmethod
    def use(cls):
        provide(TodoDAO, cls, singleton=True)()


class TodoService(Protocol):
    def save(self, todo: Todo) -> Todo:
        ...

    def find(self, todo_id: int) -> Todo:
        ...

    def update(self, todo: Todo) -> Todo:
        ...

    def delete(self, identifier: Union[int, Todo]):
        ...


@inject
@provide(TodoService)
class TodoServiceImpl(TodoService):
    _repository: TodoDAO = Marker()

    def save(self, todo: Todo) -> Todo:
        return self._repository.save(todo)

    def find(self, todo_id: int) -> Todo:
        return self._repository.find(todo_id)

    def update(self, todo: Todo) -> Todo:
        return self._repository.update(todo)

    def delete(self, identifier: Union[int, Todo]):
        return self._repository.delete(identifier)


@provide(singleton=True)
class LoggingService:
    def info(self, message: str):
        print("[INFO] " + message)


@inject
def find_todo_zero(dao: TodoDAO = Marker()):
    print(json.dumps(asdict(dao.find(0)), indent=2))


def main():
    logger = construct(LoggingService)

    if os.environ.get("DO_MOCK_DAO", "false").lower() == "true":
        MockTodoDOA.use()

    service = construct(TodoService)

    my_todo = Todo(
        None,
        "My awesome todo of DOOM"
    )

    my_todo = service.save(my_todo)

    logger.info(json.dumps(asdict(service.find(my_todo.id)), indent=2))

    find_todo_zero()


if __name__ == '__main__':
    main()
