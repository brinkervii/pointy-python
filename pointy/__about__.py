from pointy import __version__ as __version__

__version__ = __version__
__name__ = "pointy"
__description__ = "A microscopic dependency injection library"
__author__ = "BrinkerVII"
__author_email__ = "brinkervii@gmail.com"
__url__ = "https://gitlab.com/brinkervii/pointy-python"
